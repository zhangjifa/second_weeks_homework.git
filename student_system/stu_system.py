#!/usr/bin/env python3
# -*-coding:utf-8-*-
# Author:ZhangJifa


import db_setting
from db_operate import Stu_operate

TABLE = db_setting.DB_CFG["db_table"]


def findall(stu):
    '''
    :func: select student message from database
    :param stu: database operate object
    :return:
    '''
    menu = u'''
            \033[32;1m
            ---------- FIND MESSAGE ----------
            1.  查询所有学生信息
            2.  按学号查询
            3.  按名字查询
            4.  按性别查询
            5.  按班级查询
            Q.  返回上级菜单
            ----------------------------------
            \033[0m'''

    while True:
        print(menu)
        data = ""
        option = input("Please enter option:").strip()
        if option == "Q":
            break
        elif option == "1":
            sql = "select * from {}".format(TABLE)
            data = stu.findall(sql)
        elif option == "2":
            stu_id = input("Please enter student id:").strip()
            sql = "select * from {0} where id= '{1}'".format(TABLE, stu_id)
            data = stu.findall(sql)
        elif option == "3":
            stu_name = input("Please enter student name: ").strip()
            sql = "select * from {0} where name='{1}'".format(TABLE, stu_name)
            data = stu.findall(sql)
        elif option == "4":
            stu_sex = input("Please enter sex: ").strip()
            sql = "select * from {0} where sex= '{1}'".format(TABLE, stu_sex)
            data = stu.findall(sql)
        elif option == "5":
            class_id = input("Please enter class_id: ").strip()
            sql = "select * from {0} where class_id= '{1}'".format(TABLE, class_id)
            data = stu.findall(sql)
        else:
            print("Error: Invalid option ! please re-enter ...")
        if data != "":
            print("学号\t\t\t姓名\t\t\t年龄\t\t\t性别\t\t\t班级\t\t\t联系方式")
            for msg in data:
                for i in range(0, len(msg)):
                    print("{:<8}".format(msg[i]), end="\t")
                print()


def add_msg(stu):
    '''
    :func: insert student message into database
    :param stu: database operate object
    :return:
    '''
    menu = u'''
                   \033[32;1m
                   ---------- INSERT MESSAGE ----------
                   1.  开始添加学生信息
                   Q.  返回上级菜单
                   ------------------------------------
                   \033[0m'''
    while True:
        print(menu)
        option = input("Please enter option:")
        if option == "Q":
            break
        elif option == "1":
            stu_id = input("Please enter student id:").strip()
            stu_name = input("Please enter student name:").strip()
            stu_age = input("Please enter student age:").strip()
            stu_sex = input("Please enter student sex:").strip()
            class_id = input("Please enter student class:").strip()
            phone = input("Please enter student phone:").strip()
            sql_add = "insert into {0} value ('{1}','{2}','{3}','{4}','{5}','{6}')".format(
                TABLE, stu_id, stu_name, stu_age, stu_sex, class_id, phone
            )
            flag = stu.add_data(sql_add)
            if flag:
                print("Success: insert student %s is success!" % stu_id)
            else:
                print("Failed: student %s is exists!" % stu_id)
        else:
            print("Error: Invalid option ! please re-enter ...")


def delete_msg(stu):
    '''
    :func: delete student message from database
    :param stu: database operate object
    :return:
    '''
    menu = u'''
               \033[32;1m
               ---------- DELETE MESSAGE ----------
               1.  按学号删除
               2.  按姓名删除
               Q.  返回上级菜单
               ------------------------------------
               \033[0m'''

    while True:
        print(menu)
        option = input("Please enter option:")
        if option == "Q":
            break
        elif option == "1":
            stu_id = input("Please enter student id:").strip()
            sql = "delete from {0} where id='{1}'".format(TABLE, stu_id)
            flag = stu.del_data(sql)
            if flag:
                print("Success: delete student %s is success!" % stu_id)
            else:
                print("Failed: delete student %s is failed!" % stu_id)
        elif option == "2":
            stu_name = input("Please enter student name:").strip()
            sql = "delete from {0} where name= '{1}'".format(TABLE, stu_name)
            flag = stu.del_data(sql)
            if flag:
                print("Success: delete student %s is success!" % stu_name)
            else:
                print("Failed: delete student %s is failed!" % stu_name)
        else:
            print("Error: Invalid option ! please re-enter ...")


def logout(stu):
    '''
    :func: exit system
    :param stu: database operate object
    :return:
    '''
    exit("Your are logout ....")


def interactive(stu):
    '''
    :func: interact with user
    :param: student operate object
    :return:
    '''
    menu = u'''
        \033[32;1m
        ---------- STUDENT SYSTEM ----------
        1.  查询学生信息
        2.  添加学生信息
        3.  删除学生信息
        Q.  退出
        ------------------------------------
        \033[0m'''
    menu_dic = {
        '1': findall,
        '2': add_msg,
        '3': delete_msg,
        'Q': logout
    }
    while True:
        print(menu)
        option = input("Please enter option:")
        if option in menu_dic:
            menu_dic[option](stu)
            # input("Please enter any key and return to the previous menu ...")
        else:
            print("\033[31;1mError: Invalid option, please re-enter ...\033[0m")


if __name__ == "__main__":
    stu = Stu_operate()
    interactive(stu)



