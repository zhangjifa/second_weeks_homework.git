#!/usr/bin/env python3
# -*-coding:utf-8-*-
# Author:ZhangJifa


import pymysql
import db_setting

DBHOST = db_setting.DB_CFG["db_host"]
DBUSER = db_setting.DB_CFG["db_user"]
DBPASSWD = db_setting.DB_CFG["db_password"]
DBNAME = db_setting.DB_CFG["db_name"]
DBCHARSET = db_setting.DB_CFG["db_charset"]


class Stu_operate:

    def __init__(self):
        self._db_host = DBHOST
        self._db_user = DBUSER
        self._db_passwd = DBPASSWD
        self._db_name = DBNAME
        self._db_charset = DBCHARSET
        self._db = self.connection()
        if self._db:
            self._cursor = self._db.cursor()

    # connection mysql database
    def connection(self):
        db = False
        try:
            db = pymysql.connect(host=self._db_host, user=self._db_user, password=self._db_passwd,
                                 db=self._db_name, charset=self._db_charset,
                                 )
        except Exception as err:
            print("connection database failed: %s", err)
            db = False

        return db

    # insert into database
    def add_data(self, sql):
        flag = ""
        if self._db:
            try:
                self._cursor.execute(sql)
                self._db.commit()
                flag = True
            except Exception as err:
                print("insert into database failed: %s", err)
                flag = False
        return flag

    # select from database
    def findall(self, sql):
        data = ""
        if self._db:
            try:
                self._cursor.execute(sql)
                data = self._cursor.fetchall()
            except Exception as err:
                print("select database failed: %s", err)
                data = False
        return data

    # delete data from database according to student id
    def del_data(self, sql):
        flag = ""
        if self._db:
            try:
                self._cursor.execute(sql)
                self._db.commit()
                flag = True
            except Exception as err:
                print("delete database failed: %s", err)
                flag = False
        return flag

    # update data to database
    def update(self, sql):
        flag = ""
        if self._db:
            try:
                self._cursor().execute(sql)
                self._db.commit()
                flag = True
            except Exception as err:
                print("update database failed: %s", err)
                flag = False
            return flag

    # close database connection
    def __del__(self):
        self._cursor.close()
        self._db.close()
        pass



