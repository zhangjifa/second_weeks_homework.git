
import time
import random
import pygame


class Bullet:
    def __init__(self, main_screen, x, y):
        self.x = x + 53
        self.y = y
        self.screen = main_screen
        self.image = pygame.image.load("./images/pd.png")
        self.i = 1

    def display(self):
        self.screen.blit(self.image, (self.x, self.y))

    def move(self, plane_type):
        if plane_type == "hero":
            self.y -= 10
            if self.y <= -15:
                return True
        elif plane_type == "enemy":
            self.i += 1
            self.y += 2 * self.i
            if self.y >= 568:
                return True


class Player:
    def __init__(self, main_screen):
        self.x = 200
        self.y = 450
        self.image = pygame.image.load("./images/me.png")
        self.screen = main_screen
        # 定义子弹匣
        self.bullet_list = []
        # 添加爆炸特效
        self.hit = False
        self.bomb_index = 0
        self.bomb_list = []
        self.create_bomb()

    def display(self):
        # 如果被击中,就显示爆炸效果,否则显示普通的飞机效果
        if self.hit is True:
            self.screen.blit(self.bomb_list[self.bomb_index], (self.x + 35, self.y - 10 ))
            self.bomb_index += 1
        if self.bomb_index > 3:
            time.sleep(1)
            exit()
        for b in self.bullet_list:
            b.display()
            if b.move("hero"):
                self.bullet_list.remove(b)
        self.screen.blit(self.image, (self.x, self.y))

    def left_move(self):
        self.x -= 10
        if self.x <= 0:
            self.x = 0

    def right_move(self):
        self.x += 10
        if self.x >= 400:
            self.x = 400

    def up_move(self):
        self.y -= 10
        if self.y <= 0:
            self.y = 0

    def down_move(self):
        self.y += 10
        if self.y >= 490:
            self.y = 490

    def fire(self):
        self.bullet_list.append(Bullet(self.screen, self.x, self.y))
        # print(len(self.bullet_list))

    def create_bomb(self):
        # 添加爆炸图片
        self.bomb_list.append(pygame.image.load("./images/bomb0.png").convert_alpha())
        self.bomb_list.append(pygame.image.load("./images/bomb1.png").convert_alpha())
        self.bomb_list.append(pygame.image.load("./images/bomb2.png").convert_alpha())
        self.bomb_list.append(pygame.image.load("./images/bomb3.png").convert_alpha())


class Enemy:
    def __init__(self, main_screen):
        self.x = random.choice(range(408))
        self.y = -75
        self.screen = main_screen
        self.bullet_list = []
        self.bullet_time = -1
        # 添加爆炸特效
        self.bomb_index = 0
        self.bomb_list = []
        self.create_bomb()

        if random.choice(range(5)) == 1:
            self.image = pygame.image.load("./images/e0.png").convert_alpha()
        elif random.choice(range(5)) == 2:
            self.image = pygame.image.load("./images/e1.png").convert_alpha()
        else:
            self.image = pygame.image.load("./images/e2.png").convert_alpha()

    def display(self):
        for b in self.bullet_list:
            b.display()
            if b.move("enemy"):
                self.bullet_list.remove(b)
        self.screen.blit(self.image, (self.x, self.y))
        self.bullet_time += 1
        if self.bullet_time % 40 == 0:
            self.bullet_list.append(Bullet(self.screen, self.x-4, self.y + 75))

    def move(self, hero):
        self.y += 5
        if self.y >= 568:
            return True
        # 遍历所有子弹,并执行碰撞检测
        for bo in hero.bullet_list:
            if bo.x > (self.x + 12) and bo.x < (self.x + 92) and bo.y > (self.y + 20) and bo.y < (self.y + 60):
                hero.bullet_list.remove(bo)
                return True
        # 遍历所有子弹,并执行碰撞检测
        for bu in self.bullet_list:
                if bu.x > hero.x + 6 and bu.x < (hero.x + 100) and bu.y > (hero.y + 3) and bu.y < (hero.y + 43):
                    self.bullet_list.remove(bu)
                    hero.hit = True

    def create_bomb(self):
        # 添加爆炸图片
        self.bomb_list.append(pygame.image.load("./images/bomb0.png").convert_alpha())
        self.bomb_list.append(pygame.image.load("./images/bomb1.png").convert_alpha())
        self.bomb_list.append(pygame.image.load("./images/bomb2.png").convert_alpha())
        self.bomb_list.append(pygame.image.load("./images/bomb3.png").convert_alpha())

    def bomb(self):
        # 如果被击中,就显示爆炸效果,否则显示普通的飞机效果
        self.screen.blit(self.bomb_list[self.bomb_index], (self.x+35, self.y + 57))
        self.bomb_index += 1
        if self.bomb_index > 3:
            self.bomb_index = 0
