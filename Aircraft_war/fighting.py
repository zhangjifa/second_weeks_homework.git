#!/usr/bin/env python3
# -*-coding:utf-8-*-
# Author:ZhangJifa


from Player_class import *

import sys
import time
import pygame
from pygame.locals import *


def key_control(hero_temp):
    # 接收到退出事件后退出程序
    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()
    # 获取按键信息
    pressed_key = pygame.key.get_pressed()
    if pressed_key[K_LEFT] or pressed_key[K_a]:
        hero_temp.left_move()
    elif pressed_key[K_RIGHT] or pressed_key[K_d]:
        hero_temp.right_move()
    elif pressed_key[K_UP] or pressed_key[K_w]:
        hero_temp.up_move()
    elif pressed_key[K_DOWN] or pressed_key[K_s]:
        hero_temp.down_move()
    # hero_temp.check_bullet(enemy_temp)
    if pressed_key[K_SPACE]:
        hero_temp.fire()
    if pressed_key[K_ESCAPE]:
        print("quit")


def main():
    # 初始化模块，为使用硬件做准备
    pygame.init()
    # 创建一个游戏窗口
    screen = pygame.display.set_mode((512, 568), 0, 24)
    # 给游戏窗口设置标题
    pygame.display.set_caption("Aircraft war")
    # 设置游戏背景图片(加载并转换)
    background = pygame.image.load("./images/bg2.png").convert()

    hero = Player(screen)
    Enemy_list = []

    y = -968
    while True:
        # 将背景图加到窗口
        screen.blit(background, (0, y))
        y += 2
        if y >= -200:
            y = -968

        hero.display()
        key_control(hero)

        if random.choice(range(50)) == 10:
            Enemy_list.append(Enemy(screen))
        for em in Enemy_list:
            em.display()
            if em.move(hero):
                em.bomb()
                Enemy_list.remove(em)
        # 显示窗口
        pygame.display.update()
        time.sleep(0.04)


if __name__ == "__main__":
    main()
