/*
SQLyog 企业版 - MySQL GUI v8.14 
MySQL - 5.7.17-log : Database - blogdb
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`blogdb` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `blogdb`;

/*Table structure for table `blog` */

DROP TABLE IF EXISTS `blog`;

CREATE TABLE `blog` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id号',
  `title` varchar(100) NOT NULL COMMENT '标题',
  `abstract` varchar(200) NOT NULL COMMENT '摘要',
  `content` text NOT NULL COMMENT '博文内容',
  `uid` int(10) unsigned DEFAULT NULL COMMENT '用户标识',
  `pcount` int(10) unsigned DEFAULT '0' COMMENT '点赞数',
  `flag` tinyint(3) unsigned DEFAULT '0' COMMENT '状态:0新建,1发布,2删除',
  `cdate` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/*Data for the table `blog` */

insert  into `blog`(`id`,`title`,`abstract`,`content`,`uid`,`pcount`,`flag`,`cdate`) values (1,'啦啦啦啦-德玛西亚-第一季','游戏娱乐','啦啦啦啦德玛西亚,撸阿鲁啊,剑圣偷塔',1,500,1,'2015-12-23 09:14:44'),(2,'爱你一万年','流行音乐','爱你一万年,爱你经得起考验',10,1314,0,'2010-03-23 10:23:56'),(3,'老中医','花粥','姐是老中医,姐专治吹NB',8,225,3,'2014-08-24 23:21:19'),(4,'时代在召唤','中学必备','第七代校园广播体操现在开始',2,154,2,'2006-09-18 06:50:21'),(5,'Python:从入门到放弃','IT编程','Python Programming: from Rookie to Master',1,1102,0,'2018-07-20 22:22:41'),(6,'秋之黄','季节','秋天来得忒急了，民国文人笔下的北京之秋最美',5,122,2,'2012-11-02 18:37:38'),(7,'秦淮八绝','历史人物','顾横波,董小宛,卞玉京,李香君,寇白门,马湘兰,柳如是,陈圆圆',1,777,0,'2015-04-17 17:58:37'),(8,'孙悟空为什么变不好尾巴','随笔趣谈','都是孙悟空存在一颗贡高我慢之心',4,470,1,'2016-01-06 07:45:15'),(9,'苋菜','美食','五月苋,正当时;六月苋,当鸡蛋;七月苋,金不换',4,186,1,'2014-08-08 07:13:43'),(10,'健康的喝水方法','健康养生','定时喝水,早晨空腹喝水',8,224,2,'2013-03-13 18:11:26'),(11,'10月22日早盘点评','股市金融','深成指涨2.79%,报收7387点',6,78,3,'2018-10-22 09:14:19');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id号',
  `name` varchar(32) NOT NULL COMMENT '姓名',
  `email` varchar(100) DEFAULT NULL COMMENT '邮箱地址',
  `cdate` datetime DEFAULT NULL COMMENT '注册时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`cdate`) values (1,'Alice','Alice@163.com','2018-10-21 22:33:31'),(2,'Bob','Bob@qq.com','2018-10-21 22:35:23'),(3,'Coco','Coco@sina.cn','2015-06-27 13:09:54'),(4,'David','David@ict.ac.cn','2018-10-21 22:54:47'),(5,'Ella','Ella@qq.com','2018-10-21 21:09:12'),(6,'Frank','Frank@smart.cn','2017-08-21 05:34:21'),(7,'Google','Google@gmail.com','2015-03-06 11:43:24'),(8,'Hellen','Hellen@163.com','2017-08-21 05:34:21'),(9,'Isabelle','Isabelle@126.com','2015-03-06 08:29:21'),(10,'Jack','Jack@ict.ac.cn','2007-06-30 14:56:01'),(11,'Katherine','Katherine@qq.com','2010-07-06 18:56:01');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
