
import sys
import pygame
import time
import copy
import random


class Music:

    def __init__(self):
        self.bk_music = r'./material/bj.mp3'
        pygame.mixer.init()

    def start(self):
        track = pygame.mixer.music.load(self.bk_music)
        pygame.mixer.music.play()

    def stop(self):
        pygame.mixer.music.pause()

    def cont(self):
        pygame.mixer.music.unpause()


class Snake:
    def __init__(self):
        self.direction = "R"
        self.pos_list = [[42, 42]]

    def position(self):
        return self.pos_list

    def change_dire(self, direction):
        self.direction = direction

    def move(self):
        pos = len(self.pos_list) - 1
        while pos > 0:
            self.pos_list[pos] = copy.deepcopy(self.pos_list[pos - 1])
            pos -= 1
        if self.direction == 'U':
            self.pos_list[0][1] -= 14
        elif self.direction == "D":
            self.pos_list[0][1] += 14
        elif self.direction == "L":
            self.pos_list[0][0] -= 14
        elif self.direction == "R":
            self.pos_list[0][0] += 14

    def eat_food(self, food_pos):
        self.pos_list.insert(0, food_pos)


class Food:
    def __init__(self):
        self.x = random.randint(5, 35) * 14
        self.y = random.randint(5, 35) * 14
        self.food_pos = [self.x, self.y]

    def display(self):
        return self.food_pos

    def update_pos(self):
        self.food_pos[0] = random.randint(0, 39) * 14
        self.food_pos[1] = random.randint(0, 39) * 14


def main():
    # 初始化模块，为使用硬件做准备
    pygame.init()

    # 创建一个游戏窗口
    screen = pygame.display.set_mode((500, 560), 0, 32)
    # 给游戏窗口设置标题
    pygame.display.set_caption("Snake")
    # 设置游戏时钟
    clock = pygame.time.Clock()
    snake = Snake()
    music = Music()
    food = Food()
    dire = "R"
    music.start()
    while True:
        # 设置屏幕背景填充色
        screen.fill([255, 255, 255])
        # 获取事件
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_UP or event.type == pygame.K_w:
                    dire = "U"
                elif event.key == pygame.K_LEFT or event.type == pygame.K_a:
                    dire = "L"
                elif event.key == pygame.K_RIGHT or event.type == pygame.K_d:
                    dire = "R"
                elif event.key == pygame.K_DOWN or event.type == pygame.K_s:
                    dire = "D"
        # Draw the food and the snake
        food_position = copy.deepcopy(food.display())
        food_position[0] += 7
        food_position[1] += 7
        pygame.draw.circle(screen, (255, 0, 0), food_position, 10)
        snake_position = snake.position()
        for pos in snake_position:
            temp_rect = pygame.Rect(pos[0], pos[1], 14, 14)
            pygame.draw.rect(screen, (255, 0, 0), temp_rect)
        pygame.display.update()

        # 5 frame of pictures per second
        # and move the snake
        frame_rate = clock.tick(5)
        snake.change_dire(dire)
        # print(snake.direction)
        snake.move()

        # Judge whether the snake eats a food or not
        food_position = copy.deepcopy(food.display())
        snake_head = copy.deepcopy(snake.position()[0])
        if dire == 'R':
            snake_head[0] += 14
        elif dire == 'L':
            snake_head[0] -= 14
        elif dire == 'U':
            snake_head[1] -= 14
        elif dire == 'D':
            snake_head[1] += 14
        if snake_head == food_position:
            snake.eat_food(food_position)
            food.update_pos()

        # Judge whether the snake gets a collision or not
        gameover_flag = False
        snake_position = snake.position()
        snake_head = snake_position[0]
        if dire == 'R':
            if snake_head[0] > 546:
                gameover_flag = True
        elif dire == 'L':
            if snake_head[0] < 0:
                gameover_flag = True
        elif dire== 'U':
            if snake_head[1] < 0:
                gameover_flag = True
        elif dire == 'D':
            if snake_head[1] > 546:
                gameover_flag = True
        for pos in range(1, len(snake_position) - 1):
            if snake_head == snake_position[pos]:
                gameover_flag = True

        if gameover_flag:
            pygame.font.init()
            screen.fill((100, 0, 0))
            font = pygame.font.SysFont("arial", 32)
            text = font.render("Game Over!", True, (255, 0, 0))
            screen.blit(text, (200, 250))
            pygame.display.update()
            music.stop()
            while True:
                again_event = pygame.event.poll()
                if again_event.type == pygame.QUIT:
                    sys.exit()


if __name__ == "__main__":
    main()

